From: Sebastian Pipping <sebastian@pipping.org>
Date: Mon, 21 Feb 2022 15:48:32 +0100
Subject: bpo-46811: Make test suite support Expat >=2.4.5 (GH-31453)

Curly brackets were never allowed in namespace URIs
according to RFC 3986, and so-called namespace-validating
XML parsers have the right to reject them a invalid URIs.

libexpat >=2.4.5 has become strcter in that regard due to
related security issues; with ET.XML instantiating a
namespace-aware parser under the hood, this test has no
future in CPython.

References:
- https://datatracker.ietf.org/doc/html/rfc3968
- https://www.w3.org/TR/xml-names/

Also, test_minidom.py: Support Expat >=2.4.5
(cherry picked from commit 2cae93832f46b245847bdc252456ddf7742ef45e)

Co-authored-by: Sebastian Pipping <sebastian@pipping.org>

origin: https://github.com/python/cpython/commit/fdfd7a93540b0866ba42264ecb9b0a3c2286f654.patch
---
 Lib/test/test_minidom.py   | 19 +++++++++++++++++--
 Lib/test/test_xml_etree.py |  6 ------
 2 files changed, 17 insertions(+), 8 deletions(-)

diff --git a/Lib/test/test_minidom.py b/Lib/test/test_minidom.py
index ee8c041..8ebbcc4 100644
--- a/Lib/test/test_minidom.py
+++ b/Lib/test/test_minidom.py
@@ -5,10 +5,12 @@ import pickle
 from test.support import findfile
 import unittest
 
+import pyexpat
 import xml.dom.minidom
 
 from xml.dom.minidom import parse, Node, Document, parseString
 from xml.dom.minidom import getDOMImplementation
+from xml.parsers.expat import ExpatError
 
 
 tstfile = findfile("test.xml", subdir="xmltestdata")
@@ -1105,7 +1107,14 @@ class MinidomTest(unittest.TestCase):
 
         # Verify that character decoding errors raise exceptions instead
         # of crashing
-        self.assertRaises(UnicodeDecodeError, parseString,
+        # on debian this is always True due to backport
+        if pyexpat.version_info >= (2, 4, 5) or True:
+            self.assertRaises(ExpatError, parseString,
+                    b'<fran\xe7ais></fran\xe7ais>')
+            self.assertRaises(ExpatError, parseString,
+                    b'<franais>Comment \xe7a va ? Tr\xe8s bien ?</franais>')
+        else:
+            self.assertRaises(UnicodeDecodeError, parseString,
                 b'<fran\xe7ais>Comment \xe7a va ? Tr\xe8s bien ?</fran\xe7ais>')
 
         doc.unlink()
@@ -1551,7 +1560,13 @@ class MinidomTest(unittest.TestCase):
         self.confirm(doc2.namespaceURI == xml.dom.EMPTY_NAMESPACE)
 
     def testExceptionOnSpacesInXMLNSValue(self):
-        with self.assertRaisesRegex(ValueError, 'Unsupported syntax'):
+        # on debian this is always True due to backport
+        if pyexpat.version_info >= (2, 4, 5) or True:
+            context = self.assertRaisesRegex(ExpatError, 'syntax error')
+        else:
+            context = self.assertRaisesRegex(ValueError, 'Unsupported syntax')
+
+        with context:
             parseString('<element xmlns:abc="http:abc.com/de f g/hi/j k"><abc:foo /></element>')
 
     def testDocRemoveChild(self):
diff --git a/Lib/test/test_xml_etree.py b/Lib/test/test_xml_etree.py
index 6c7616b..9fe092a 100644
--- a/Lib/test/test_xml_etree.py
+++ b/Lib/test/test_xml_etree.py
@@ -1648,12 +1648,6 @@ class BugsTest(unittest.TestCase):
                 b"<?xml version='1.0' encoding='ascii'?>\n"
                 b'<body>t&#227;g</body>')
 
-    def test_issue3151(self):
-        e = ET.XML('<prefix:localname xmlns:prefix="${stuff}"/>')
-        self.assertEqual(e.tag, '{${stuff}}localname')
-        t = ET.ElementTree(e)
-        self.assertEqual(ET.tostring(e), b'<ns0:localname xmlns:ns0="${stuff}" />')
-
     def test_issue6565(self):
         elem = ET.XML("<body><tag/></body>")
         self.assertEqual(summarize_list(elem), ['tag'])
